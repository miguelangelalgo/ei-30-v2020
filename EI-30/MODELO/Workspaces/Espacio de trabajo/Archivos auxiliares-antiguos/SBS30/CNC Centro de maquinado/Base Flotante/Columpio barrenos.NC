%
O0000(COLUMPIO BARRENOS)
(DATE=DD-MM-YY - 09-06-15 TIME=HH:MM - 17:13)
(MCX FILE - D:\DOCUMENTOS\INVENTOR\SBS30\MASTERCAM\COLUMPIO.MCX-8)
(NC FILE - D:\DOCUMENTOS\INVENTOR\SBS30\CNC CENTRO DE MAQUINADO\COLUMPIO BARRENOS.NC)
(MATERIAL - ALUMINUM INCH - 2024)
( T16 | 3/16 FLAT ENDMILL | H16 )
N100 G20
N102 G0 G17 G40 G49 G80 G90
( PARED EN CARA POSTERIOR BASE )
( INICIA LADO IZQUIERDO )
N104 T16 M6
N106 G0 G90 G54 X1.375 Y-.47 S2800 M3
N108 G43 H16 Z.1
N110 M8
N112 G99 G83 Z-.27 R.1 Q.05 F2.
N114 G80
N116 X1.3428 Z.2
N118 G1 Z-.045 F6.16
N120 G2 X1.375 Y-.4378 I.0322 J0.
N122 X1.4072 Y-.47 I0. J-.0322
N124 X1.375 Y-.5022 I-.0322 J0.
N126 X1.3428 Y-.47 I0. J.0322
N128 G1 Z-.09
N130 G2 X1.375 Y-.4378 I.0322 J0.
N132 X1.4072 Y-.47 I0. J-.0322
N134 X1.375 Y-.5022 I-.0322 J0.
N136 X1.3428 Y-.47 I0. J.0322
N138 G1 Z-.135
N140 G2 X1.375 Y-.4378 I.0322 J0.
N142 X1.4072 Y-.47 I0. J-.0322
N144 X1.375 Y-.5022 I-.0322 J0.
N146 X1.3428 Y-.47 I0. J.0322
N148 G1 Z-.18
N150 G2 X1.375 Y-.4378 I.0322 J0.
N152 X1.4072 Y-.47 I0. J-.0322
N154 X1.375 Y-.5022 I-.0322 J0.
N156 X1.3428 Y-.47 I0. J.0322
N158 G1 Z-.225
N160 G2 X1.375 Y-.4378 I.0322 J0.
N162 X1.4072 Y-.47 I0. J-.0322
N164 X1.375 Y-.5022 I-.0322 J0.
N166 X1.3428 Y-.47 I0. J.0322
N168 G1 Z-.27
N170 G2 X1.375 Y-.4378 I.0322 J0.
N172 X1.4072 Y-.47 I0. J-.0322
N174 X1.375 Y-.5022 I-.0322 J0.
N176 X1.3428 Y-.47 I0. J.0322
N178 G0 Z.1
M9
G0 X-0.5 Y3
M05
M00
(POSICION DERECHA)
G0
M8
N180 S3000 M3
N182 G54 X.375 Y-.47 Z.1
N184 G99 G83 Z-.27 R.1 Q.05 F2.
N186 G80
N188 X.4073 Z.2
N190 G1 Z-.045 F6.16
N192 G2 X.375 Y-.5023 I-.0323 J0.
N194 X.3427 Y-.47 I0. J.0323
N196 X.375 Y-.4377 I.0323 J0.
N198 X.4073 Y-.47 I0. J-.0323
N200 G1 Z-.09
N202 G2 X.375 Y-.5023 I-.0323 J0.
N204 X.3427 Y-.47 I0. J.0323
N206 X.375 Y-.4377 I.0323 J0.
N208 X.4073 Y-.47 I0. J-.0323
N210 G1 Z-.135
N212 G2 X.375 Y-.5023 I-.0323 J0.
N214 X.3427 Y-.47 I0. J.0323
N216 X.375 Y-.4377 I.0323 J0.
N218 X.4073 Y-.47 I0. J-.0323
N220 G1 Z-.18
N222 G2 X.375 Y-.5023 I-.0323 J0.
N224 X.3427 Y-.47 I0. J.0323
N226 X.375 Y-.4377 I.0323 J0.
N228 X.4073 Y-.47 I0. J-.0323
N230 G1 Z-.225
N232 G2 X.375 Y-.5023 I-.0323 J0.
N234 X.3427 Y-.47 I0. J.0323
N236 X.375 Y-.4377 I.0323 J0.
N238 X.4073 Y-.47 I0. J-.0323
N240 G1 Z-.27
N242 G2 X.375 Y-.5023 I-.0323 J0.
N244 X.3427 Y-.47 I0. J.0323
N246 X.375 Y-.4377 I.0323 J0.
N248 X.4073 Y-.47 I0. J-.0323
N250 G0 Z.1
N252 M9
N254 M5
N256 G91 G28 Z0.
N258 G28 X0. Y0.
N260 M30
%
