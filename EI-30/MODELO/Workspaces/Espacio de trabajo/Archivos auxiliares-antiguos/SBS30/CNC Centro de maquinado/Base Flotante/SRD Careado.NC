%
O0000(SRD CAREADO)
(DATE=DD-MM-YY - 18-11-15 TIME=HH:MM - 15:06)
(MCX FILE - F:\DOCUMENTOS\INVENTOR\SBS30\MASTERCAM CENTRO DE MAQUINADO\BASE FLOTANTE\SOPORTE DELGADO.MCX-8)
(NC FILE - F:\DOCUMENTOS\INVENTOR\SBS30\CNC CENTRO DE MAQUINADO\BASE FLOTANTE\SRD CAREADO.NC)
(MATERIAL - ALUMINUM INCH - 2024)
( T5 | 1/2 FLAT ENDMILL | H5 )
N100 G20
N102 G0 G17 G40 G49 G80 G90
( PRIMER PASO )
( MATERIAL ANGULO DE 3 X 1 1/4 X 3/16 X 5.1 )
( COLOCAR UN POCO SALIDO PARA CAREADO )
( COLOCAR ENTRE PLACAS DE 1/2 )
N104 T5 M6
N106 G0 G90 G54 X-.255 Y-3.6125 
S2500 M3
N108 G43 H5 Z.25
N110 M8
N112 Z.2
N114 G1 Z-.2375 F20.
N116 Y-3.3125 F30.
N118 Y-.75
N120 Y-.45
N122 G0 Z.0125
N124 Z.2
N126 X-.25 Y-3.6125
N128 G1 Z-.2375 F20.
N130 Y-3.3125 F30.
N132 Y-.75
N134 Y-.45
N136 G0 Z.0125
N138 Z.25
N140 X5.255
N142 Z.2
N144 G1 Z-.2375 F20.
N146 Y-.75 F30.
N148 Y-3.3125
N150 Y-3.6125
N152 G0 Z.0125
N154 Z.2
N156 X5.25 Y-.45
N158 G1 Z-.2375 F20.
N160 Y-.75 F30.
N162 Y-3.3125
N164 Y-3.6125
N166 G0 Z.25
N168 X-.25 Y-3.1625
N170 Z.2
N172 G1 Z-.2167 F20.
N174 Y-3.3125 F30.
N176 Y-3.5
N178 Y-3.65
N180 G0 Z.0333
N182 Y-3.1625
N184 Z-.0167
N186 G1 Z-.4333 F20.
N188 Y-3.3125 F30.
N190 Y-3.5
N192 Y-3.65
N194 G0 Z-.1833
N196 Y-3.1625
N198 Z-.2333
N200 G1 Z-.65 F20.
N202 Y-3.3125 F30.
N204 Y-3.5
N206 Y-3.65
N208 G0 Z-.4
N210 Y-3.1625
N212 Z-.45
N214 G1 Z-.8667 F20.
N216 Y-3.3125 F30.
N218 Y-3.5
N220 Y-3.65
N222 G0 Z-.6167
N224 Y-3.1625
N226 Z-.6667
N228 G1 Z-1.0833 F20.
N230 Y-3.3125 F30.
N232 Y-3.5
N234 Y-3.65
N236 G0 Z-.8333
N238 Y-3.1625
N240 Z-.8833
N242 G1 Z-1.3 F20.
N244 Y-3.3125 F30.
N246 Y-3.5
N248 Y-3.65
N250 G0 Z-1.05
N252 Z.25
N254 X5.25
N256 Z.2
N258 G1 Z-.2167 F20.
N260 Y-3.5 F30.
N262 Y-3.3125
N264 Y-3.1625
N266 G0 Z.0333
N268 Y-3.65
N270 Z-.0167
N272 G1 Z-.4333 F20.
N274 Y-3.5 F30.
N276 Y-3.3125
N278 Y-3.1625
N280 G0 Z-.1833
N282 Y-3.65
N284 Z-.2333
N286 G1 Z-.65 F20.
N288 Y-3.5 F30.
N290 Y-3.3125
N292 Y-3.1625
N294 G0 Z-.4
N296 Y-3.65
N298 Z-.45
N300 G1 Z-.8667 F20.
N302 Y-3.5 F30.
N304 Y-3.3125
N306 Y-3.1625
N308 G0 Z-.6167
N310 Y-3.65
N312 Z-.6667
N314 G1 Z-1.0833 F20.
N316 Y-3.5 F30.
N318 Y-3.3125
N320 Y-3.1625
N322 G0 Z-.8333
N324 Y-3.65
N326 Z-.8833
N328 G1 Z-1.3 F20.
N330 Y-3.5 F30.
N332 Y-3.3125
N334 Y-3.1625
N336 G0 Z.25
N338 M09
N340 M5
N342 G91 G28 Z0.
N344 G28 X0. Y0.
N346 M30
%
